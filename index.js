const { Pool } = require('pg')
const express = require('express')
const bodyParser = require('body-parser')
const Excel = require('exceljs')
require('dotenv').config()

const port = parseInt(process.env.PORT, 10) || 5432
const host = '10.11.247.5'
const username = 'postgres'
const password = 'postgres'
const database = 'casaamerica'

const app = express()

app.use(bodyParser.urlencoded({ extended: true }))

app.get('/', (req, res) => res.sendFile(`${__dirname}/index.html`))

app.post('/dates', (req, res) => {
  const date_array = req.body.dates.split(' - ')

  start_date_arr = date_array[0].split('/')
  end_date_arr = date_array[1].split('/')

  startDate =
    start_date_arr[2] +
    '/' +
    start_date_arr[1] +
    '/' +
    start_date_arr[0] +
    ' 00:00:00'
  endDate =
    end_date_arr[2] +
    '/' +
    end_date_arr[1] +
    '/' +
    end_date_arr[0] +
    ' 00:00:00'

  getClientes(req, res, startDate, endDate)
})

app.post('/acordos', (req, res) => {
  const date_array = req.body.acordos.split(' - ')

  start_date_arr = date_array[0].split('/')
  end_date_arr = date_array[1].split('/')

  startDate =
    start_date_arr[2] +
    '/' +
    start_date_arr[1] +
    '/' +
    start_date_arr[0] +
    ' 00:00:00'
  endDate =
    end_date_arr[2] +
    '/' +
    end_date_arr[1] +
    '/' +
    end_date_arr[0] +
    ' 00:00:00'

  getAcordos(req, res, startDate, endDate)
})

app.listen(8080, () => console.log(`Listening on 9191`))

const pool = new Pool({
  user: username,
  password: password,
  host: host,
  port: port,
  database: database,
  max: 5,
})

// pool.connect();

const getClientes = async (req, res, startDate, endDate) => {
  try {
    console.log('Connected')

    const results = await pool.query(
      "SELECT distinct(pessoas.cpf),pessoas.nome FROM financeiro_movimentacoes INNER JOIN pessoas ON pessoas.id_pessoa = financeiro_movimentacoes.id_cliente WHERE financeiro_movimentacoes.data_pagamento IS NULL AND financeiro_movimentacoes.data_vencimento >= $1 AND financeiro_movimentacoes.data_vencimento <= $2 AND financeiro_movimentacoes.discriminador = 'EntradaVenda' AND financeiro_movimentacoes.tipo_entrada_venda = 'P' AND financeiro_movimentacoes.situacao in (0,10,7,9,5) order by pessoas.cpf",
      [startDate, endDate]
    )

    if (results.rows.length != 0) {
      console.table(results.rows)
      await generateExcel(req, res, results.rows)
    } else {
      // await pool.end()
      // console.log("Disconnected");
      return res.send('A busca não retornou nenhum client')
    }

    // await pool.end()
    // console.log("Disconnected");
  } catch (e) {
    console.log(e)
  }
}
const getAcordos = async (req, res, startDate, endDate) => {
  try {
    console.log('Connected')

    const results = await pool.query(
      "SELECT pessoas.cpf, REPLACE(ROUND(sum(financeiro_movimentacoes.valor)::numeric,2)::text,'.',',') as Divida,pessoas.nome FROM financeiro_movimentacoes INNER JOIN pessoas ON pessoas.id_pessoa = financeiro_movimentacoes.id_cliente WHERE financeiro_movimentacoes.data_pagamento IS NULL AND financeiro_movimentacoes.data_vencimento >= $1 AND financeiro_movimentacoes.data_vencimento <= $2 AND financeiro_movimentacoes.discriminador = 'EntradaVenda' AND financeiro_movimentacoes.tipo_entrada_venda = 'A' AND financeiro_movimentacoes.id_acordo IS NOT NULL AND financeiro_movimentacoes.duplicata LIKE '%A%' AND financeiro_movimentacoes.situacao = 0 GROUP BY pessoas.cpf,pessoas.nome",
      [startDate, endDate]
    )

    if (results.rows.length != 0) {
      console.table(results.rows)
      await generateExcel(req, res, results.rows)
    } else {
      // await pool.end()
      // console.log("Disconnected");
      return res.send('A busca não retornou nenhum cliente')
    }

    // await pool.end()
    // console.log("Disconnected");
  } catch (e) {
    console.log(e)
  }
}

const generateExcel = async (req, res, results) => {
  try {
    var workbook = new Excel.Workbook()
    var worksheet = workbook.addWorksheet('Clientes Inadimplentes')

    worksheet.columns = [
      { header: 'CPF', key: 'cpf', width: 32 },
      { header: 'Nome', key: 'nome', width: 64 },
    ]

    var i
    for (i = 0; i < results.length; i++) {
      worksheet.addRow({
        index: i,
        cpf: results[i].cpf,
        nome: results[i].nome,
      })
    }

    await downloadExcel(workbook, res)
  } catch (e) {
    console.log(e)
  }
}

async function downloadExcel(workbook, response) {
  try {
    var fileName = 'clientesInad.xlsx'

    response.setHeader(
      'Content-Type',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )
    response.setHeader(
      'Content-Disposition',
      'attachment; filename=' + fileName
    )

    await workbook.xlsx.write(response)
    console.log('File sent!')
    response.end()
  } catch (e) {
    console.log(e)
  }
}
